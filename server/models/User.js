import mongoose from "mongoose";
import bcrypt from "bcrypt";
const saltRounds = 10;
import jwt from "jsonwebtoken";

//user model in database MongoDb
const userSchema = mongoose.Schema({
    //user name
    name: {
        type:String,
        maxlength:50
    },
    //user email
    email: {
        type:String,
        trim:true,
        unique: 1 
    },
    //user password
    password: {
        type: String,
        minglength: 5
    },
    //lastname user
    lastname: {
        type:String,
        maxlength: 50
    },
    //cart of products, selected by user
    cart: {
        type: Array,
        default: []
    },
    //image according to user
    image: String,
    //user token
    token : {
        type: String,
    }
})

//recording function of model data of registered user in to database MongoDb 
userSchema.pre('save', function( next ) {
    var user = this;
    
    if (user.isModified('password')){    
        //encrypting the user's password
        bcrypt.genSalt(saltRounds, function(err, salt){
            //first step of encrypting failed
            if(err) return next(err);
            
            bcrypt.hash(user.password, salt, function(err, hash){
                //encrypting failed
                if(err) return next(err);
                //encrypting turned out and we fixing encrypted user password 
                user.password = hash 
                next()
            })
        })
    } else {
        next()
    }
});

//comparing password function
userSchema.methods.comparePassword = function(plainPassword,cb){
    //password plainPassword comparing with password this.password
    bcrypt.compare(plainPassword, this.password, function(err, isMatch){
        if (err) return cb(err);
        //result of password comparison isMatch sending to router of user authorization
        cb(null, isMatch)
    })
}

//the function of forming user token
userSchema.methods.generateToken = function(cb) {
    var user = this;
    //creating token according to packet jwt and in token loading ID of authorized user user._id
    var token =  jwt.sign(user._id.toHexString(),'secret')
    //inputing created token in model of database user
    user.token = token;
    //saving database user model with created token in database MongoDb
    user.save(function (err, user){
        if(err) return cb(err)
        cb(null, user);
    })
}

//the function of finding data according to token
userSchema.statics.findByToken = function (token, cb) {
    var user = this;
    //finding user ID according to token token
    jwt.verify(token,'secret',function(err, decode){
        //ID decode was found, finding itself user
        user.findOne({"_id":decode, "token":token}, function(err, user){
            //the user could not be found
            if(err) return cb(err);
            //the user was found
            cb(null, user);
        })
    })
}

//fixing user model in data base MongoDb
const User = mongoose.model('User', userSchema);
export default User; 