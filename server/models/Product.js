import mongoose from "mongoose";
const Schema = mongoose.Schema;

//product model in data base MongoDb
const productSchema = mongoose.Schema({
    //user which selected corresponding product
    writer: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    //name product
    title: {
        type: String,
        maxLength: 50
    },
    //description product
    description: {
        type: String
    },
    //price product
    price: {
        type: Number,
        default: 0
    },
    //images product
    images: {
        type: Array,
        default: []
    },
    //the countries of creating product
    continents: {
        type: Number,
        default: 1
    }
}, { timestamp: true })

productSchema.index({
    title: 'text',
    description: 'text'
}, {
    weights: {
        name: 5,
        description: 1
    }
})

//fixing model product in database MongoDb
const Product = mongoose.model('Product', productSchema);
export default Product;