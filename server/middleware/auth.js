import User from '../models/User.js';

//the function of verifying the existence of an authorized user
let auth = (req, res, next) => {  
  //define token formed in cookies
  let token = req.cookies.w_auth;

  //in data base on server we finding defined token
  User.findByToken(token, (err, user) => 
  {
    //an unexpected error occurred
    if (err) throw err;

    //there is no user then we sending on client-application negative result
    if (!user)
      return res.json({
        isAuth: false,
        error: true
      });

    //we sending the token and the user himself to the routers, which corresponds to it
    req.token = token;
    req.user = user;
    next();
  });
  
};

export default auth;