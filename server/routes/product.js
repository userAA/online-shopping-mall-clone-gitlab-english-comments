import express from "express";
const router = express.Router();
import multer from "multer";

//finding model data of product according to data base MongoDb
import Product from "../models/Product.js";
//finding function of verification of the existence of an authorized user
import auth from "../middleware/auth.js";

//image saving process function
var storage = multer.diskStorage({
    //the path to the folder in the server project in which will save new images
    destination: (req, file, cb) => {
        cb(null, 'uploads/');
    },
    //file name of new image
    filename: (req, file, cb) => {
        cb(null, `${Date.now()}_${file.originalname}`)
    },
    //available extension of images
    fileFilter: (req, file, cb) => {
        const ext = path.extname(file.originalname);
        if (ext !== '.jpg' || ext !== '.png') {
            return cb(res.status(400).end('only jpg, png, mp4 are allowed'), false)
        }
        cb(null, true)
    }
})

//the function of saving images of new product from authorized user in project server
var upload = multer({storage: storage}).single("file")

//=================================
//             Product
//=================================

//router request for saving images of new product from authorized user in project server
router.post("/uploadImage", auth, (req, res) => {
    //saving images of new product from authorized user in project server
    upload(req, res, err => {
        if (err) return res.json({success: false, err});
        return res.json({success: true, image: res.req.file.path, filename: res.req.file.filename})
    })
});

//router request for loading product to server according to authorized user
router.post("/uploadProduct", auth, (req, res) => {
    //creating model itself product according to initial data req.body
    const product = new Product(req.body);

    //saving product in data base on server
    product.save((err) => {
        if (err) return res.status(400).json({success: false, err});
        return res.status(200).json({success: true});
    })
});

//router of loading product with server according to available parameters
router.post("/getProducts", (req, res) => {
    //sorting order of downloading products
    let order  = "desc";
    //downloaded products are sorted by their ID
    let sortBy = "_id";
    //maximum number of downloaded product
    let limit  = req.body.limit ? parseInt(req.body.limit) : 100;
    //the quantity of product, which are skiped before them downloading
    let skip   = parseInt(req.body.skip); 
    //the list of prices or countries by which we filtering the downloaded products
    let findArgs = {};
    //according to label searchTerm we filtering indicated products based them name or description
    let term = req.body.searchTerm;

    for (let key in req.body.filters) {
        if (req.body.filters[key].length > 0) {
            if (key === "price") {
                //compiling the list of prices according to which we filtering downloading products
                findArgs[key] = {
                    $gte: req.body.filters[key][0],
                    $lte: req.body.filters[key][1]
                }
            } else {
                //compiling of list of countries according to which filter downloading products
                findArgs[key] = req.body.filters[key];
            }
        }
    }

    if (term) {
        //the term label exists
        //products are filtered by the findArgs array
        Product.find(findArgs)
            //products are filtered according to them name or description based from label term
            .find({$text: {$search: term}})
            //outputing full information about the originator of the product
            .populate("writer")
            //sorted downloaded products according to them ID in order decreasing
            .sort([[sortBy, order]])
            //defined number of products we skiping
            .skip(skip)
            //defined number of product we downloading
            .limit(limit)
            .exec( (err, products) => {
                //downloading of products was not successful
                if (err) return res.status(400).json({success: false, err});
                //downloading the products was successful
                res.status(200).json({success: true, products, postSize: products.length});
            })  
    } else {
        //the term label does not exist
        //products are filtered by the findArgs array
        Product.find(findArgs)
            //outputing full information about originator product
            .populate("writer")
            //sorting downloading products according them ID in order decreasing
            .sort([[sortBy, order]])
            //defined number of products we skiping
            .skip(skip)
            //defined number of products we downloading
            .limit(limit)
            .exec( (err, products) => {
                //the downloading of products is turned out to be not successful
                if (err) return res.status(400).json({success: false, err});
                //the downloading products is turned out to be successful
                res.status(200).json({success: true, products, postSize: products.length});
            })  
    }
});

//router request for receiving information about products in cart of authorized user according them IDs
router.get("/products_by_id", (req, res) => {
    //finding type data type
    let type = req.query.type;
    //finding data in kind of array IDs of products productIds
    let productIds = req.query.id;

    if (type === "array") {
        let ids = req.query.id.split(',');
        productIds = [];
        productIds = ids.map(item => {
            return item;
        })
    }

    //seeking information about products with IDs in array productIds
    Product.find({'_id': {$in: productIds}})
    //outputing full information about user which selected indicated products
    .populate('writer').exec((err, product) => {  
        //the search of specified information is turned out to be failed
        if (err) return res.status(400).send(err);
        //the search of specified information is turned out to be successfull
        return res.status(200).send(product)
    })
});

export default router;