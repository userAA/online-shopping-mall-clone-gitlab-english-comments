import express from "express";
const router = express.Router();

//finding model data of user according to data base MongoDb
import User from "../models/User.js";
//finding model data of product according to data base MongoDb
import Product from "../models/Product.js";
//finding function of verification existence of authorized user
import auth from "../middleware/auth.js";

//=================================
//             User
//=================================

//router requesting the existence of an authorized user
router.get("/auth", auth, (req, res) => {
    //in the case of successfull result from function auth sending all obtained information from this function about
    //authorized user on client-application of this project
    res.status(200).json({
        _id: req.user._id,
        isAuth: true,
        email: req.user.email,
        name: req.user.name,
        lastname: req.user.lastname,
        image: req.user.image,
        cart: req.user.cart,
    });
});

//router user registration
router.post("/register", (req, res) => {
    //creating model of regestering user according to data base MongoDb
    const user = new User(req.body);

    //saving created model of registering user in to data base
    user.save((err, doc) => {
        //the recording failed
        if (err) return res.json({ success: false, err });
        //the recording passed
        return res.status(200).json({
            success: true
        });
    });
});

//router of user authorization
router.post("/login", (req, res) => {
    //Seeking user in user model data base according to known email req.body.email
    User.findOne({ email: req.body.email }, (err, user) => {
        if (!user)
            return res.json({
                loginSuccess: false,
                message: "Auth failed, email not found"
            });

        //user was found now we comparing him password with known password req.body.password
        user.comparePassword(req.body.password, (err, isMatch) => {
            //comparing of passwords did not pass
            if (!isMatch)
                return res.json({ loginSuccess: false, message: "Wrong password" });
            //the comparing of passwords is turned out to be succesfull, then creating user token
            user.generateToken((err, user) => {
                //the token could not be generated
                if (err) return res.status(400).send(err);
                //token managed to form
                res.cookie("w_authExp", user.tokenExp);
                res.cookie("w_auth", user.token).status(200)
                .json({
                    //sending to client-application the information about succesfull user authorization
                    loginSuccess: true, userId: user._id
                });
            });
        });
    });
});

//router request on clearing information according to authorized user
router.get("/logout", auth, (req, res) => {
    //clear information about authorized user with him ID req.user._id (removing his token).
    User.findOneAndUpdate({ _id: req.user._id }, { token: "", tokenExp: "" }, (err, doc) => {
        //to clear information about authorized user with ID req.user._id failed
        if (err) return res.json({ success: false, err });
        //indicated action passed succesfully
        return res.status(200).send({
            success: true
        });
    });
});

//router request for adding new product in cart of authorized user
router.get('/addToCart', auth, (req, res) => 
{
    //seeking user according ID req.user._id 
    User.findOne({ _id: req.user._id }, (err, userInfo) => 
    {
        //there is full information according to authorized user userInfo
        //sorting through the products in cart of authorized user
        let duplicate = false;
        userInfo.cart.forEach((item) => 
        {
            //there is already the product in cart which we adding
            if (item.id == req.query.productId) 
            {
                duplicate = true;
            }
        })

        if (duplicate) 
        {
            //adding already available product in cart
            User.findOneAndUpdate(
                { _id: req.user._id, "cart.id": req.query.productId },
                //the quantity of products with ID req.query.productId increase on units
                { $inc: { "cart.$.quantity": 1 } },
                { new: true },
                (err, userInfo) => 
                {
                    //the adding of already available product is turned out to be not succesfull
                    if (err) return res.json({ success: false, err });
                    //the adding of already available product is turned out to be successfull
                    res.status(200).json(userInfo.cart)
                }
            )
        } 
        else 
        {
            //adding new product in cart
            User.findOneAndUpdate(
                { _id: req.user._id },
                {
                    $push: {
                        cart: {
                            id: req.query.productId,
                            quantity: 1,
                            date: Date.now()
                        }
                    }
                },
                { new: true },
                (err, userInfo) => 
                {
                    //the adding of new product is turned out to be successfull
                    if (err) return res.json({ success: false, err });
                    //the adding of new product is turned out to be not succesfull
                    res.status(200).json(userInfo.cart)
                }
            )
        }
    })
});

//router request for removing series of identical products from cart of authorized user
router.get('/removeFromCart', auth, (req, res) => 
{
    User.findOneAndUpdate(
        {_id: req.user._id},
        //carring out the specified process of removing in cart of products of authorized user with ID req.user._id
        {
            "$pull": {"cart": {"id": req.query._id}}
        },
        {new: true},
        (err, userInfo) => 
        {
            //receiving the cart of products according to authorized user
            let cart = userInfo.cart;
            //defining the array of IDS series of identical products corresponding to cart of products cart
            let array = cart.map(item => 
            {
                return item.id
            })
            //receiving full information about product besides from created array array
            Product.find({'_id' : {$in: array}})
            .populate('writer')
            .exec((err, cartDetail) => {
                return res.status(200).json({
                    //outputing to client-application full received information
                    cartDetail,
                    //outputing on client-application received cart of products
                    cart
                })
            })
        }
    )
})

//router request for receiving information about all products in cart of authorized user
router.get('/userCartInfo', auth, (req,res) => {
    //determining the userInfo information for an authorized user based on his identifier req.user._id
    User.findOne(
        {_id: req.user._id},
        (err, userInfo) => {
            //finding cart of products of authorized user from all his received information
            let cart = userInfo.cart;
            //defining the array IDs according all products in cart of authorizes user
            let array = cart.map(item =>{
                return item.id
            })
            //collecting information about all products in cart of authorized user
            Product.find({'_id': {$in: array}})
                //defining information about user which selected product
                .populate('writer')
                .exec((err, cartDetail) => {
                    //requested information was not received
                    if (err) return res.status(400).send(err);
                    //requested information was received
                    return res.status(200).json({success: true, cartDetail, cart})
                })
        }
    )
})

export default router;