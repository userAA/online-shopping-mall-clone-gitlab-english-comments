import axios from 'axios';
import {
    //finding label of user authorization
    LOGIN_USER,
    //finding label of user registration
    REGISTER_USER,
    //finding the label of the request for verification of existence authorized user
    AUTH_USER,
    //finding the label of the request for clearing data on server about authorized user
    //(removing his token from his data base on server)
    LOGOUT_USER,
    //finding the label of the request for adding new product in cart of authorized user
    ADD_TO_CART_USER,
    //finding the label of the request for receiving full information about all products in cart of authorized user
    GET_CART_ITEMS_USER,
    //finding the label of the request for removing series identical products in cart of authorized user
    REMOVE_CART_ITEM_USER 
} from './types';
import { USER_SERVER } from '../components/Config.js';

//user registration request function
export function registerUser(dataToSubmit){
    const request = axios.post(`${USER_SERVER}/register`,dataToSubmit).then(response => response.data);
    //the data which is being fitted to stotre, received in result of application specified request
    return {
        type: REGISTER_USER,
        payload: request
    }
}

//user authorization request function
export function loginUser(dataToSubmit){
    const request = axios.post(`${USER_SERVER}/login`,dataToSubmit).then(response => response.data);
    //data which is being fitted in store, received in result of application specified request
    return {
        type: LOGIN_USER,
        payload: request
    }
}

//the function of the request to verify the existence of an authorized user
export function auth(){
    const request = axios.get(`${USER_SERVER}/auth`).then(response => response.data);
    //received data about authorized user, which is being fitted in store
    return {
        type: AUTH_USER,
        payload: request
    }
}

//the function of the request to delete information about an authorized user
export function logoutUser(){
    const request = axios.get(`${USER_SERVER}/logout`).then(response => response.data);
    //the data which us being fitted in store, received in result of application specified request
    return {
        type: LOGOUT_USER,
        payload: request
    }
}

//the function of the request to add new product about authorized user
export function addToCart(_id) {
    const request = axios.get(`${USER_SERVER}/addToCart?productId=${_id}`).then(response => response.data);
    //the data which is being fitted in store, received in result of application specified request
    return {
        type: ADD_TO_CART_USER,
        payload: request
    }
}

//the function of the request for receiving information about products in cart of authorized user according to them the IDs
export function getCartItems(cartItems, userCart) {
    const request = axios.get(`/api/product/products_by_id?id=${cartItems}&type=array`)
        .then(response => 
        {
            userCart.forEach(cartItem => 
            {
                response.data.forEach((productDetail, i) => 
                {
                    if (cartItem.id === productDetail._id) 
                    {
                        //setting correct quantity of products in cart
                        response.data[i].quantity = cartItem.quantity;
                    }
                })
            })
            return response.data;
        });
    //the data which is being fitted in store, received in result of application specified request
    return {
        type: GET_CART_ITEMS_USER,
        payload: request
    }
}

//the function of the request to remove the series of identical products in cart of authorized user
export function removeCartItem(id) {
    const request = axios.get(`/api/users/removeFromCart?_id=${id}`)
        .then(response => {
            response.data.cart.forEach(item => {
                response.data.cartDetail.forEach((k,i) => {
                    if (item.id === k._id) {
                        //setting correct quantity of products in cart
                        response.data.cartDetail[i].quantity = item.quantity;
                    }
                })
            })
            return response.data;
        });
    //the data which is being fitted in store, received in result of application specified request
    return {
        type: REMOVE_CART_ITEM_USER,
        payload: request
    }
}