//the label of the user authorization request
export const LOGIN_USER = 'login_user';
//the label of the user registration request
export const REGISTER_USER = 'register_user';
//the label of the request to verify the existence of an authorized user
export const AUTH_USER = 'auth_user';
//the label of the request to clear the data on server about authorized user (removing his token from his data base on server)
export const LOGOUT_USER = 'logout_user';
//the label of the request to add new product in cart of authorized user
export const ADD_TO_CART_USER = 'add_to_cart_user';
//the label of the request to receive full information about all products in cart of authorized user
export const GET_CART_ITEMS_USER = 'get_cart_items_user';
//the label of the request to remove series of identical products in cart of authorized user
export const REMOVE_CART_ITEM_USER = 'remove_cart_item_user';