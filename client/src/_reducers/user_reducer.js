import {
    //finding user authorization label
    LOGIN_USER,
    //finding user registration label
    REGISTER_USER,
    //finding the request label for verification of user autorized existence
    AUTH_USER,
    //finding the request label for clearing data on server about authorized user
    //(removing his token from his data base on server)
    LOGOUT_USER,
    //finding the request label for adding new product in cart of authorized user
    ADD_TO_CART_USER,
    //finding the request label for receiving full information about all products in cart of authorized user
    GET_CART_ITEMS_USER,
    //finding the request label for removing series of identical products in cart of authorized user
    REMOVE_CART_ITEM_USER
} from '../_actions/types';
 
function user_reducer (state={},action){
    switch(action.type){
        //finding data from store according to label REGISTER_USER
        case REGISTER_USER:
            return {...state, register: action.payload }
        //finding data from store acording to label LOGIN_USER
        case LOGIN_USER:
            return { ...state, loginSucces: action.payload }
        //finding data from store about authorized user according to label AUTH_USER
        case AUTH_USER:
            return {...state, userData: action.payload }
        //from the store, finding the result obtained as a result of applying a request according to label LOGOUT_USER
        case LOGOUT_USER:
            return {...state }
        //from the store, finding the result obtained as a result of applying a request according to label ADD_TO_CART_USER
        //and creating full cart of products according to authorized user
        case ADD_TO_CART_USER:
            return {
                ...state, userData: {
                    ...state.userData,
                    cart: action.payload
                }
            }
        //from the store, finding the result obtained as a result of applying a request according to label GET_CART_ITEMS_USER
        case GET_CART_ITEMS_USER: {
            return {...state, cartDetail: action.payload}            
        }
        //from the store, finding the result obtained as a result of applying a request according to label REMOVE_CART_ITEM_USER
        case REMOVE_CART_ITEM_USER: {
            return {
                ...state,
                //full information about remaining products in cart of authorized user
                cartDetail: action.payload.cartDetail,
                userData: {
                    ...state.userData,
                    //series of remaining identical products in cart of authorized user
                    cart: action.payload.cart                    
                }                
            }
        }
        default:
            return state;
    }
}

export default user_reducer;