import React from "react";
import moment from "moment";
import { Formik } from 'formik';
import * as Yup from 'yup';
//finding function of request on new user registration
import { registerUser } from "../../../_actions/user_actions";
//finding hook useDispatch
import { useDispatch } from "react-redux";

import { Form, Input, Button} from 'antd';

//layout template of all page of user registration
const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 8 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 16 },
  },
};

//layout template of location of button request for user registration
const tailFormItemLayout = {
  wrapperCol: {
    xs: {
      span: 24,
      offset: 0,
    },
    sm: {
      span: 16,
      offset: 8,
    },
  },
};

function RegisterPage(props) {
  const dispatch = useDispatch();
  return (

    <Formik
      //initial state data according to registering user
      initialValues={{
        email: '',
        lastName: '',
        name: '',
        password: '',
        confirmPassword: ''
      }}
      //the schema verification of all data according to registering user
      validationSchema={Yup.object().shape({
        //verification structure of user name
        name: Yup.string().required('Name is required'),
        //verification structure of user last name
        lastName: Yup.string().required('Last Name is required'),
        //verification structure of user email
        email: Yup.string().email('Email is invalid').required('Email is required'),
        //verification structure of user password
        password: Yup.string().min(6, 'Password must be at least 6 characters').required('Password is required'),
        //verification structure of confirming user password
        confirmPassword: Yup.string().oneOf([Yup.ref('password'), null], 'Passwords must match').required('Confirm Password is required')
      })}

      //making up request on user registration
      onSubmit={(values, { setSubmitting }) => {
        setTimeout(() => {
          //created data of registering user
          let dataToSubmit = {
            //created email
            email: values.email,
            //created password
            password: values.password,
            //created name
            name: values.name,
            //created last name
            lastname: values.lastname,
            //created image according registering user
            image: `http://gravatar.com/avatar/${moment().unix()}?d=identicon`
          };

          //implementing request on user registration according his created data
          dispatch(registerUser(dataToSubmit)).then(response => {
            if (response.payload.success) {
              //user registration passed successfully
              props.history.push("/login");
            } else {
              //user registration failed
              alert(response.payload.err.errmsg)
            }
          })

          setSubmitting(false);
        }, 500);
      }}
    >
      {props => {
        const {
          values,
          touched,
          errors,
          isSubmitting,
          handleChange,
          handleBlur,
          handleSubmit
        } = props;
        return (
          <div className="app">
            <h2>Sign up</h2>
            <Form style={{ minWidth: '375px' }} {...formItemLayout} onSubmit={handleSubmit} >
              {/*Area entering of user name*/}
              <Form.Item required label="Name">
                <Input
                  id="name"
                  placeholder="Enter your name"
                  type="text"
                  value={values.name}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  className={
                    errors.name && touched.name ? 'text-input error' : 'text-input'
                  }
                />
                {/*Fixing incorrect settings of user name */}
                {errors.name && touched.name && (
                  <div className="input-feedback">{errors.name}</div>
                )}
              </Form.Item>
              {/*Area entering of user last name*/}
              <Form.Item required label="Last Name">
                <Input
                  id="lastName"
                  placeholder="Enter your Last Name"
                  type="text"
                  value={values.lastName}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  className={
                    errors.lastName && touched.lastName ? 'text-input error' : 'text-input'
                  }
                />
                {/*Fixing incorrect settings of user last name */}
                {errors.lastName && touched.lastName && (
                  <div className="input-feedback">{errors.lastName}</div>
                )}
              </Form.Item>
              {/*Area entering email*/}
              <Form.Item required label="Email">
                <Input
                  id="email"
                  placeholder="Enter your Email"
                  type="email"
                  value={values.email}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  className={
                    errors.email && touched.email ? 'text-input error' : 'text-input'
                  }
                />
                {/*Fixing incorrect settings of user email */}
                {errors.email && touched.email && (
                  <div className="input-feedback">{errors.email}</div>
                )}
              </Form.Item>
              {/*Area entering of user password*/}
              <Form.Item required label="Password">
                <Input
                  id="password"
                  placeholder="Enter your password"
                  type="password"
                  value={values.password}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  className={
                    errors.password && touched.password ? 'text-input error' : 'text-input'
                  }
                />
                {/*Fixing incorrect settings of user password */}
                {errors.password && touched.password && (
                  <div className="input-feedback">{errors.password}</div>
                )}
              </Form.Item>
              {/*Area entering of confirming user password*/}
              <Form.Item required label="Confirm" hasFeedback>
                <Input
                  id="confirmPassword"
                  placeholder="Enter your confirmPassword"
                  type="password"
                  value={values.confirmPassword}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  className={
                    errors.confirmPassword && touched.confirmPassword ? 'text-input error' : 'text-input'
                  }
                />
                {/*Fixing incorrect settings of repeating user password */}
                {errors.confirmPassword && touched.confirmPassword && (
                  <div className="input-feedback">{errors.confirmPassword}</div>
                )}
              </Form.Item>
              {/*Area of location button for implementing request on registration*/}
              <Form.Item {...tailFormItemLayout}>
                <Button onClick={handleSubmit} type="primary" disabled={isSubmitting}>
                  Submit
                </Button>
              </Form.Item>
            </Form>
          </div>
        );
      }}
    </Formik>
  );
};

export default RegisterPage;