import React, { useState } from 'react'
import { Typography, Button, Form, Input } from 'antd';
//finding the page of loading file how on server so and on cient-application
import FileUpload from '../../utils/FileUpload'
import Axios from 'axios';

const { Title } = Typography;
const { TextArea } = Input;

//the list of countries, which correspond to creating of products 
const Continents = [
    {key: 1, value: "Africa"},
    {key: 2, value: "Europe"},
    {key: 3, value: "Asia"},
    {key: 4, value: "North America"},
    {key: 5, value: "South America"},
    {key: 6, value: "Australia"},
    {key: 7, value: "Antarctica"}
]

//page for creating a new product by an authorized user
function UploadProductPage(props) {
    //the name of new product
    const [TitleValue, setTitleValue] = useState("");
    //the description of new product
    const [DescriptionValue, setDescriptionValue] = useState("");
    //the price of new product
    const [PriceValue, setPriceValue] = useState(0);
    //the country of creating new product
    const [ContinentValue, setContinentValue] = useState(1);

    //The images of new product
    const [Images, setImages] = useState([]);

    //fixing the name of product
    const onTitleChange = (event) => {
        setTitleValue(event.currentTarget.value);
    }

    //fixing the description of product
    const onDescriptionChange = (event) => {
        setDescriptionValue(event.currentTarget.value);
    }

    //fixing the price of product
    const onPriceChange = (event) => {
        setPriceValue(event.currentTarget.value);
    }

    //fixing the country of creating product
    const onContinentsSelectChange = (event) => {
        setContinentValue(event.currentTarget.value)
    }

    //fixing of images new product
    const updateImages = (newImages) => {
        setImages(newImages)
    }

    //the action of implementing request according to loading new product on server
    const onSubmit = (event) => {
        event.preventDefault();
        if (!TitleValue || !DescriptionValue || !PriceValue || !ContinentValue || !Images) {
            return alert('fill all the fields first!')
        } 
        //the data about product selected by authorized user
        const variables = {
            //user ID, which selected product
            writer: props.user.userData._id,
            //name product
            title: TitleValue,
            //description product
            description: DescriptionValue,
            //price product
            price: PriceValue,
            //image of product
            images: Images,
            //the country of creating product
            continents: ContinentValue
        }
        //the request on loading of new product on server in model of products according to data base MongoDb
        Axios.post('/api/product/uploadProduct', variables).then(response => {
            if (response.data.success) 
            {
                //the loading of new product on server passed succesfully
                alert('Product Successfully Uploaded');
                props.history.push('/');
            } 
            else 
            {
                //the loading of new product on server failed
                alert('Failed to upload Product')
            }
        })
    }

    return (
        <div style={{maxWidth: '700px', margin: '2rem auto'}}>
            <div style = {{textAlign:'center', marginBottom: '2rem'}}>
                <Title level={2}>Upload Travel Product</Title>
            </div>
            
            <Form onSubmit={onSubmit}>
                {/* Here we upload the product image */}
                <FileUpload refreshFunction={updateImages}/>
                {/* Input field of name product */}
                <br/>
                <br/>
                <label>Title</label>
                <Input
                    onChange={onTitleChange}
                    value={TitleValue}
                />
                <br/>
                <br/>
                {/* Input field of description product */}
                <label>Description</label>
                <TextArea
                    onChange={onDescriptionChange}
                    value={DescriptionValue}
                />
                <br/>
                <br/>
                {/* Field settings of price product */}
                <label>Price($)</label>
                <Input
                    onChange={onPriceChange}
                    value={PriceValue}
                    type="number" 
                />
                <br/>
                <br/>
                {/* Selection list of country creating product */}
                <select onChange={onContinentsSelectChange}>
                    {Continents.map(item  => (
                        <option key={item.key} value={item.key}>{item.value}</option>
                    ))}
                </select>
                <br/>
                <br/>
                {/* The button of implementing request for loading new product on server */}
                <Button
                    onClick = {onSubmit}
                >
                    Submit
                </Button>
            </Form>
        </div>
    )
}

export default UploadProductPage;