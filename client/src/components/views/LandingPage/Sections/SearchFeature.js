import React, {useState} from 'react'
import {Input} from 'antd'

const {Search} = Input;

//setting component of text label for search products on server
function SearchFeature(props) {

    //the desired label
    const [SearchTerms, setSearchTerms] = useState("")

    const onChangeSearch = (event) => {
        //fixing desired label
        setSearchTerms(event.currentTarget.value);
        //sending desired label via props to the destination
        props.refreshFunction(event.currentTarget.value);
    }

    return (
        <div>
            <Search
                value = {SearchTerms}
                onChange = {onChangeSearch}
                placeholder="Search By Typing..."
            />
        </div>
    )
}

export default SearchFeature;