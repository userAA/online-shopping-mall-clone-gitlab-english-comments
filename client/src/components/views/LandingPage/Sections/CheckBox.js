import React, {useState} from 'react'
import { Checkbox, Collapse } from 'antd';

const {Panel} = Collapse;

//the page of representing ticks with countries of creating products
function CheckBox(props) {

    const [Checked, setChecked] = useState([]);

    //activate each tick
    const handleToggle = (value) => {
        const currentIndex = Checked.indexOf(value); 
        const newChecked   = [...Checked];

        if (currentIndex === -1) {
            newChecked.push(value);
        } else {
            newChecked.splice(currentIndex, 1);
        }

        //the tick is activated
        setChecked(newChecked);
        //activation of each tick is sent to the destination
        props.handleFilters(newChecked)
    }

    // setting the list of ticks
    const renderCheckboxLists = () => {
        return (
            props.list && props.list.map((value, index) => (
                <React.Fragment key={index}>
                    {/* Itself tick */}
                    <Checkbox
                        onChange = {() => handleToggle(value._id)}
                        type="checkbox"
                        checked={Checked.indexOf(value._id) === -1 ? false : true}
                    />
                    {/* The name of tick */}
                    <span>{value.name}</span>
                </React.Fragment>
            ))
        )
    }

    return (
        //itself list of ticks
        <div>
            <Collapse defaultActiveKey={['0']}>
                <Panel header="Continents" key="1">
                    {renderCheckboxLists()}
                </Panel>
            </Collapse>
        </div>
    )
}

export default CheckBox;