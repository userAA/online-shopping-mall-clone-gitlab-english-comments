import React, {useState} from 'react'
import {Collapse, Radio} from 'antd'
const {Panel} = Collapse;

//the page of representing Radio buttons, characterizing ranges of prices of products
function RadioBox(props) {

    const [Value, setValue] = useState('0');

    //setting the list Radio buttons according corresponding names from props
    const renderRadioBox = () => (
        props.list && props.list.map((value) => (
            <Radio key={value._id} value={`${value._id}`}>{value.name}</Radio>
        ))
    )

    //the function of changing state of which Radio button
    const handleChange = (event) => {
        setValue(event.target.value);
        //the state of corresponding Radio buttons via props sending to the destination
        props.handleFilters(event.target.value )
    }

    return (
        //itself list of Radio buttons
        <div>
            <Collapse defaultActiveKey={['0']}>
                <Panel header="price" key="1">
                    <Radio.Group onChange={handleChange} value={Value}>
                        {renderRadioBox()}
                    </Radio.Group>
                </Panel>
            </Collapse>
        </div>
    )
}

export default RadioBox;