import React, {useEffect, useState, useCallback} from 'react'
import Axios from 'axios'
import {Row, Col, Card} from 'antd'
import Icon from '@ant-design/icons';
import ImageSlider from '../../utils/ImageSlider'
//finding the page of representing ticks with countries of creating products
import CheckBox from './Sections/CheckBox'
//finding the page of representing Radio buttons, characterizing ranges of prices of products
import RadioBox from './Sections/RadioBox'
//finding lists of countries and prices
import { continents, price } from './Sections/Datas'
//finding component of tasking text label of search products on server
import SearchFeature from './Sections/SearchFeature'
const {Meta} = Card;

function LandingPage() {
    //list of products downloaded with server
    const [Products, setProducts] = useState([]);
    //the quantity of products, which needed to skip in data base on server before them downloading with server
    const [Skip, setSkip]         = useState(0);
    //maximum known quantity of products downloading with server
    let Limit       = 6;
    const [PostSize, setPostSize] = useState();

    //Filtration of products
    const [Filters, setFilters] = useState({
        //The products are being distributed according to countries
        continents: [],
        //The products are being distributed according to price
        price: []
    })

    //receiving products from data base on server, the quantity of which is limited by structures variables
    const getProducts = useCallback(
        (variables) => 
        {
            Axios.post('/api/product/getProducts', variables)
            .then(response => {
                //downloading products from the server turned out to be successful
                if (response.data.success) {
                    if (variables.loadMore) {
                        //adding additional part downloaded with server products in addition to available
                        setProducts([...Products,...response.data.products]);
                    } else {
                        //fixing first part of products downloaded with server
                        setProducts(response.data.products);
                    }
                    //fixing the quantity of downloaded products
                    setPostSize(response.data.postSize);
                } else {
                    alert('Failed to fetch product datas')
                }
            })
        },
        [setProducts, Products]
    );

    useEffect(() => {
        const variables = {
            //how many products do we skip before downloading them from the server
            skip: Skip,
            //the maximum number of products that we download from the server
            limit: Limit
        }

        //making up the request for downloading of products according to indicated parameters
        getProducts(variables);
    }, [Skip, Limit, getProducts])

    //if we want to download with server ever more products then have been already downloaded
    const onLoadMore = () => {
        //we skip so many products before downloading them from the server
        let skip = Skip + Limit;

        const variables = {
            skip: skip,
            limit: Limit,
            //this flag speak about, that download additional quantity of products in addition to that which has been already downloaded
            loadMore: true
        }

        //making up request for downloading products according to indicated parameters
        getProducts(variables);
        //setting new quantity of products which we skip before them downloading with server
        setSkip(skip)
    }

    //representing the carts of downloaded products
    const renderCards = Products.map((product, index) => {
        return (
            <Col lg={6} md={8} xs={24}>
                {/*Images of products */}
                <Card
                    hoverable={true}
                    cover={
                        <a href={`/product/${product._id}`}>
                            {/*In this slider outputing all images of product */}
                            <ImageSlider images={product.images}/>
                        </a>
                    }
                >
                    <Meta
                        //name product
                        title = {product.title}
                        //price product
                        description = {`$${product.price}`}
                    />
                </Card>
            </Col>
        )
    })

    //the representing function of downloaded products taking into account them filtration according to prices or countries
    const showFilteredResults = (filters) => {
        const variables = {
            //we skip so many products
            skip: 0,
            //we download such maximum quantity products 
            limit: Limit,
            //arrays of filtering products before downloading them
            filters: filters
        }
        //starting up proccess of downloading products
        getProducts(variables);
        //reseting to zero the quantity of products, which skiping before them downloading from server
        setSkip(0);
    }

    //the function of definition of range prices according to will filter the products, downloading with server
    const handlePrice = (value) => {
        const data = price;

        let array  = [];

        for (let key in data) {
            if (data[key]._id === parseInt(value,10)) {
                array = data[key].array;
            }
        }
        return array;
    }

    const handleFilters = (filters, category) => {
        const newFilters = {...Filters};
        newFilters[category] = filters;

        if (category === "price") {
            //defining the range of prices according to will filter products, downloading with server
            let priceValues = handlePrice(filters);
            //fixing the range of prices according to will filter products, downloading with server
            newFilters[category] = priceValues;
        }

        //representing downloaded products taking into account them filtration according to prices or countries
        showFilteredResults(newFilters);
        //fixing countries and range of prices on the basis of which will filter products downloading with server
        setFilters(newFilters);
    }

    //the function of displaying downloaded products, taking into account their filtering by prices or countries 
    //and their search tags by name or description
    const updateSearchTerms = (newSearchTerm) => {
        const variables = {
            //the quantity of products, which we skip before them downloading
            skip: 0,
            //downloading such maximum quantity of products 
            limit: Limit,
            //arrays of filtering products before downloading them
            filters: Filters,
            //the label of filtration products according to them name or description
            searchTerm: newSearchTerm
        }
        //reset to zero the quantity of downloaded products, which skiping
        setSkip(0)
        //starting up the proccess of downloading products
        getProducts(variables)
    }

    return (
        <div style={{ width: '75%', margin: '3rem auto' }}>
            <div style={{textAlign:'center'}}>
                <h2> Let's Travel Anywhere <Icon type="rocket"/></h2>
            </div>

            <Row gutter={[16,16]}>
                <Col lg={12} xs={24}>
                    {/*The list of countries according to which will implement the filtration of downloaded with server products*/}
                    <CheckBox
                        list={continents}
                        handleFilters={filters => handleFilters(filters,"continents")}
                    />
                </Col>
                <Col lg={12} xs={24}>
                    {/*The list of ranges prices according to which will implement filtration of downloaded with server products */}
                    <RadioBox
                        list={price}
                        handleFilters={filters => handleFilters(filters,"price")}
                    />
                </Col>
            </Row>

            {/* Window settings of search needed products according to it description */ }
            <div style={{display: 'flex', justifyContent: 'flex-end', margin: '1rem auto'}}>
                <SearchFeature
                    refreshFunction={updateSearchTerms}
                />
            </div>

            {/*Here we show already downloaded products, if they already exist */}
            {Products.length === 0 ? 
                //There is no downloaded with server products
                <div  style={{display: 'flex', height: '300px', justifyContent: 'center', alignItems: 'center'}}>
                    <h2>No post yet...</h2>
                </div>
                :
                //There is downloaded with server products
                <div>
                    <Row gutter={[16,16]}>
                        {renderCards}
                    </Row>
                </div>
            }
            <br/><br/>

            {/*If on server remained still products besides those which has been already downloaded so them one can to download */}
            {PostSize >= Limit && 
                <div style={{display: 'flex', justifyContent: 'center'}}>
                    <button onClick={onLoadMore}>Load More</button>
                </div>
            }
        </div>
    )
}

export default LandingPage;