import React, { useState } from "react";
import { withRouter } from "react-router-dom";
import { loginUser } from "../../../_actions/user_actions";
import { Formik } from 'formik';
import * as Yup from 'yup';
import { Form, Input, Button, Typography } from 'antd';
import Icon from '@ant-design/icons';
//finding hook useDispatch
import { useDispatch } from "react-redux";

const { Title } = Typography;

function LoginPage(props) {
  const dispatch = useDispatch();

  //the state of formed errors at result of process authorization
  const [formErrorMessage, setFormErrorMessage] = useState('')

  return (
    <Formik
      //initial state data according to authorization of registered user
      initialValues={{
        email:  '',
        password: '',
      }}
      //the schema verification of total data according to authorization of registered user
      validationSchema={Yup.object().shape({
        //the structure verification of user email
        email: Yup.string().email('Email is invalid').required('Email is required'),
        //the structure verification of user password
        password: Yup.string().min(6, 'Password must be at least 6 characters').required('Password is required'),
      })}

      //making request on authorization of registered user
      onSubmit={(values, { setSubmitting }) => {
        setTimeout(() => {
          //created state of data for authorization of registered user
          let dataToSubmit = {
            //created email
            email: values.email,
            //created password
            password: values.password
          };

          //implementing request on authorization of registered user according to corresponding data
          dispatch(loginUser(dataToSubmit))
            .then(response => {
              if (response.payload.loginSuccess) {
                //success authorization of registered user
                //in window.localStorage saving ID of registered user response.payload.userId
                window.localStorage.setItem('userId', response.payload.userId);
                //switching on central page project, when proccess of authorization of registered user passed successfully
                props.history.push("/");
              } else {
                //Authorization of registered user failed
                setFormErrorMessage('Check out your Account or Password again')
              }
            })
            .catch(err => {
              //Authorization of registered user failed
              setFormErrorMessage('Check out your Account or Password again')
              //After three seconds clearing all errors, received at result of failed proccess of authorization of registered user
              setTimeout(() => {
                setFormErrorMessage("")
              }, 3000);
            });
          setSubmitting(false);
        }, 500);
      }}
    >
      {props => {
        const {
          values,
          touched,
          errors,
          isSubmitting,
          handleChange,
          handleBlur,
          handleSubmit
        } = props;
        return (
          <div className="app">

            <Title level={2}>Log In</Title>
            <form onSubmit={handleSubmit} style={{ width: '350px' }}>
              {/*Area entering of user email*/}
              <Form.Item required>
                <Input
                  id="email"
                  prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
                  placeholder="Enter your email"
                  type="email"
                  value={values.email}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  className={
                    errors.email && touched.email ? 'text-input error' : 'text-input'
                  }
                />
                {/*Fixing incorrect entering of user email */}
                {errors.email && touched.email && (
                  <div className="input-feedback">{errors.email}</div>
                )}
              </Form.Item>
              {/*Area entering of user password*/}
              <Form.Item required>
                <Input
                  id="password"
                  prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
                  placeholder="Enter your password"
                  type="password"
                  value={values.password}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  className={
                    errors.password && touched.password ? 'text-input error' : 'text-input'
                  }
                />
                {/*Fixing incorrect entering of user password */}
                {errors.password && touched.password && (
                  <div className="input-feedback">{errors.password}</div>
                )}
              </Form.Item>
              {/*Outputing message about error at the event of failed authorization of registered user*/}
              {formErrorMessage && (
                <label ><p style={{ color: '#ff0000bf', fontSize: '0.7rem', border: '1px solid', padding: '1rem', borderRadius: '10px' }}>{formErrorMessage}</p></label>
              )}

              <Form.Item>
                <div>
                  {/*The button of activation proccess of authorization of registered user*/}
                  <Button type="primary" htmlType="submit" className="login-form-button" style={{ minWidth: '100%' }} disabled={isSubmitting} onSubmit={handleSubmit}>
                    Log in
                  </Button>
                </div>
                Or
                {/*Transmit on page of user registration*/} 
                <a href="/register">register now!</a>
              </Form.Item>
            </form>
          </div>
        );
      }}
    </Formik>
  );
};

export default withRouter(LoginPage);