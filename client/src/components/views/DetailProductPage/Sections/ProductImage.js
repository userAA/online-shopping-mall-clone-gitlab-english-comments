import React, {useEffect, useState} from 'react'
import ImageGallery from 'react-image-gallery'

//product image display component
function ProductImage(props) {

    //product images
    const [Images, setImages] = useState([]);

    useEffect(() => {
        if ( props.detail.images && props.detail.images.length > 0 ) {
            let images = [];
            //downloading images of product from props.detail.images
            props.detail.images && props.detail.images.forEach(item => {
                images.push({
                    original: `http://localhost:3001/${item}`,
                    thumbnail: `http://localhost:3001/${item}`
                })
            })
            //fixing product images
            setImages(images)
        }
    },[props.detail])

    return (
        //depicting product images
        <div>
            <ImageGallery items={Images}/>
        </div>
    )
}

export default ProductImage;