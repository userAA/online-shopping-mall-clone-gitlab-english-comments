import React, {useEffect, useState} from 'react'
import {Button, Descriptions} from 'antd'

//the componemt of representing full information about product
function ProductInfo(props) {
    //information about product
    const [Product, setProduct] = useState({});

    useEffect(() => {
        //fixing information about product from props
        setProduct(props.detail)
    }, [props.detail])

    //adding product in cart of authorized user via function addToCart from props
    const addToCarthandler = () => {
        props.addToCart(props.detail._id)
    }

    return (
        <div>
            <Descriptions title="Product Info">
                {/*Price product */}
                <Descriptions.Item label="Price">{Product.price}</Descriptions.Item>
                {/*Description product */}
                <Descriptions.Item label="Description">{Product.description}</Descriptions.Item>
            </Descriptions>
            <br/>
            <br/>
            <br/>
            <div style={{display: 'flex', justifyContent: 'center'}}>
                {/*The button of adding product in cart according to authorized user */}
                <Button size="large" shape="round" type="danger" onClick = {addToCarthandler}>
                    Add to Cart
                </Button>
            </div>
        </div>
    )
}

export default ProductInfo;