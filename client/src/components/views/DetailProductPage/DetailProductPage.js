import React, {useEffect, useState} from 'react'
import Axios from 'axios'
import {Col, Row} from 'antd'

//finding component of representing product image
import ProductImage from './Sections/ProductImage';
//finding component of representing detail information about product
import ProductInfo from './Sections/ProductInfo';
//finding function of implementing request according to server on adding product in cart products according to authorized user 
import {addToCart} from '../../../_actions/user_actions';
//finding hook useDispatch 
import {useDispatch} from 'react-redux'

//the page of outputing detail information about product
function DetailProductPage(props) {
    const dispatch = useDispatch();
    //finding product ID 
    const productId = props.match.params.productId;
    //downloaded product according to ID productId
    const [Product, setProduct] = useState([]);

    useEffect(() => {
        //the request on downloading with server information about product with ID productId
        Axios.get(`/api/product/products_by_id?id=${productId}&type=single`).then(response => {
            //fixing downloaded information about indicated product in the case of successfull request
            setProduct(response.data[0])
        })
    }, [productId])

    //adding product to the cart of authorized user
    const addToCartHandler = (productId) => {
        //implementing request according to server for adding product in to cart of products according to authorized user
        dispatch(addToCart(productId))
    }

    return (
        <div className="postPage" style={{width: '100%', padding: '3rem 4rem'}}>
            <div style={{display: 'flex', justifyContent: 'center'}}>
                {/*Name product */}
                <h1>{Product.title}</h1>
            </div>
            <br/>
            <Row gutter={[16,16]}>
                <Col lg={12} xs={24}>
                    {/*Showing product images */}
                    <ProductImage detail={Product}/>
                </Col>
                <Col lg={12} xs={24}>
                    {/*Showing detail information about product */}
                    <ProductInfo
                        //the function af adding product in cart of authorized user
                        addToCart={addToCartHandler} 
                        //the detail information about product
                        detail={Product}
                    />
                </Col>
            </Row>
        </div>
    )
}

export default DetailProductPage;