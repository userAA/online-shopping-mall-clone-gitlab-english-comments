/* eslint-disable jsx-a11y/anchor-is-valid */
import React from 'react';
import { Menu, Badge } from 'antd';
import Icon from '@ant-design/icons';
import axios from 'axios';
import { USER_SERVER } from '../../../Config';
import { withRouter } from 'react-router-dom';
import { useSelector } from "react-redux";

function RightMenu(props) {
  //finding from store total information about authorized user
  const user = useSelector(state => state.user)

  const logoutHandler = () => {
    //request on cleaning data of authorized user
    axios.get(`${USER_SERVER}/logout`).then(response => {
      if (response.status === 200) {
        //swirching on page of user authorization 
        props.history.push("/login");
      } else {
        //cleaning data of authorized user failed
        alert('Log Out Failed')
      }
    });
  };

  if (user.userData && !user.userData.isAuth) {
    //the event, when user non authorized or non registered 
    return (
      <Menu mode={props.mode}>
        {/*Switching on page user authorization */}
        <Menu.Item key="mail">
          <a href="/login">Signin</a>
        </Menu.Item>
        {/*Switching on page user registration */}
        <Menu.Item key="app">
          <a href="/register">Signup</a>
        </Menu.Item>
      </Menu>
    )
  } else {
    return (
      <Menu mode={props.mode}>
        {/*Switching on page of creating new product according to photo by authorized user */}
        <Menu.Item key="upload">
          <a href="/product/upload">Upload</a>
        </Menu.Item>

        {/*Switching on page of representing cart products, selected by authorized user */}
        <Menu.Item key="cart">
          {/*user.userData.cart.length - quantity products in cart */}
          <Badge count={user.userData && user.userData.cart.length}>
            <a href="/user/cart" style={{marginRight: -22, color: "#667777"}}>
              <Icon type="shopping-cart" style={{fontSize: 30, marginBottom: 4}}/>
            </a>
          </Badge>
        </Menu.Item>
        {/*Switching on page of cleaning data according authorized user */}
        <Menu.Item key="logout">
          <a onClick={logoutHandler}>Logout</a>
        </Menu.Item>
      </Menu>
    )
  }
}

export default withRouter(RightMenu);