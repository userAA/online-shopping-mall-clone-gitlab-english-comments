import React from 'react';
import { Menu } from 'antd';

//left menus
function LeftMenu(props) {
  return (
    <Menu mode={props.mode}>
    {/*Switching on central page */}
    <Menu.Item key="mail">
      <a href="/">Home</a>
    </Menu.Item>
  </Menu>
  )
}

export default LeftMenu