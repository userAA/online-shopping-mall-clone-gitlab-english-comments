import React, { useEffect, useState, useCallback} from 'react'
//finding hook useDiapatch
import { useDispatch } from 'react-redux';
import { 
    //finding request function for receiving information about products in cart of authorized user according to them IDs
    getCartItems,
    //finding request function for removing of series of identical products in cart of authorized user  
    removeCartItem
} from '../../../_actions/user_actions';
//finding the display component of the products themselves in the basket
import UserCardBlock from './Sections/UserCardBlock';
import { Empty} from 'antd' 
import Axios from 'axios';

function CartPage(props) {
    const dispatch = useDispatch();
    //Full cost of products in cart of authorized user
    const [Total, setTotal] = useState(0);
    //The flag of representing full gold of products in cart
    const [ShowTotal, setShowTotal] = useState(false);

    useEffect(() => {   
        //set array of products IDs in the entire cart of authorized user  
        let cartItems = [];
        
        //props.user.userData - full information about authorized user
        //props.user.userData.cart - full information about cart products of authorized user
        if (props.user.userData && props.user.userData.cart) {
            if (props.user.userData.cart.length > 0) {  
                //creating array of IDs products in the entire cart of authorized user          
                props.user.userData.cart.forEach(item => {
                    cartItems.push(item.id)
                });
                //implementing request for receiving information about products in cart of authorized user according them IDs
                dispatch(getCartItems(cartItems, props.user.userData.cart))
            }
        }
    }, [props.user.userData, dispatch])

    //calculation function of full gold of products in the entire cart of authorized user
    const calculateTotal = useCallback(
        (cartDetail) => 
        {
            //calculating full gold of products in the entire cart of authorized user
            let total = 0;
            cartDetail && cartDetail.forEach(item => {
                if (item.quantity !== undefined){
                    total += parseInt(item.price, 10) * item.quantity;
                }
            })
    
            if (total > 0) {
                //fix full gold of products in the entire cart of authorized user
                setTotal(total)
            }
            //activate flag of representing full cost of products in the entire cart of authorized user
            setShowTotal(true)
        },
        []
    );

    useEffect(() => {
        if (props.user.cartDetail && props.user.cartDetail.length > 0) {
            //starting calculating the full cost of products across the entire basket
            calculateTotal(props.user.cartDetail)
        }
    }, [props.user.cartDetail, calculateTotal])

    //making up process of removing series of identical products in cart of authorized user
    const removeFromCart = (productId) => {
        //implementing request for removing series of identical products in cart of authorized user
        dispatch(removeCartItem(productId))
        .then(() => {
            //implementing request for receiving content in cart of authorized user
            Axios.get('/api/users/userCartInfo')
                .then(response => {
                    //the specified request was successful
                    if (response.data.success) 
                    {
                        if (response.data.cartDetail.length <= 0) {
                            //the cart of authorized user was empty
                            setShowTotal(false)
                        } 
                        else 
                        {
                            //the cart of authorized user is not empty, calculating full cost of remaining products in it
                            calculateTotal(response.data.cartDetail)
                        }
                    }
                    else
                    //the indicated request was not successfull
                    {
                        alert('Failed to get cart info')
                    }
                })
        })
    }

    return (
        <div style={{ width: '85%', margin: '3rem auto' }}>
            <h1>My Cart</h1>
            <div>
                {/*the component of representing of themeselfes products in cart */}
                <UserCardBlock 
                    //temeselfes products in cart
                    products={props.user.cartDetail}
                    //the function of removing products from cart
                    removeItem={removeFromCart}
                />

                {ShowTotal ?
                    //outputing full cost of products in total cart of authorized user
                    <div style={{marginTop: '3rem'}}>
                        <h2>Total amount: ${Total}</h2>
                    </div>
                    :
                    //there is no the products in the cart of authorized user
                    <div style={{
                        width: '100%', display: 'flex', flexDirection: 'column',
                        justifyContent: 'center'
                    }}>
                        <br/>
                        <Empty description={false}/>
                        <p>No Items In the Cart</p>
                    </div>
                }
            </div>
        </div>
    )
}

export default CartPage;