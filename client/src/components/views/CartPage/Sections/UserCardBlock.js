import React from 'react'

//the component of representing of full information about page of products of authorized user
function UserCardBlock(props) {
    //the function of displaying the product image from the specified basket
    const renderCartImage = (images) => {
        if (images.length > 0) {
            let image = images[0];
            return `http://localhost:3001/${image}`
        }
    }

    //the function of displaying all products from the specified basket
    const renderItems = () => (
        //pre-checking the existence of the specified products in props
        props.products && props.products.map(product => (
            <tr key={product._id}>
                <td>
                    {/*Image product */}
                    <img 
                        style={{ width: '70px' }} 
                        alt="product"  
                        src={renderCartImage(product.images)} />
                </td> 
                {/*The quantity of units product in indicated cart */}
                <td>{product.quantity} EA</td>
                {/*The price of corresponding product */}
                <td>$ {product.price} </td>
                <td>
                    {/*The button of removing of all units of specified products from basket */}
                    <button 
                        onClick = {() => props.removeItem(product._id)}
                    >
                        Remove 
                    </button> 
                </td>
            </tr>
        ))
    )


    return (
        <div>
            <table>
                {/*Page headering of output all products of cart */}
                <thead>
                    <tr>
                        <th>Product Image</th>
                        <th>Product Quantity</th>
                        <th>Product Price</th>
                        <th>Remove from Cart</th>
                    </tr>
                </thead>
                {/*The implementation of the withdrawal of products in the specified basket*/}
                <tbody>
                    {renderItems()}
                </tbody>
            </table>
        </div>
    )
}

export default UserCardBlock;