import React, { Suspense } from 'react';
import { Route, Switch } from "react-router-dom";
import authFunction from "../hoc/auth";

// finding pages and components for this project
import LandingPage from "./views/LandingPage/LandingPage.js";
import LoginPage from "./views/LoginPage/LoginPage.js";
import RegisterPage from "./views/RegisterPage/RegisterPage.js";
import NavBar from "./views/NavBar/NavBar";
import Footer from "./views/Footer/Footer";
import UploadProductPage from './views/UploadProductPage/UploadProductPage';
import DetailProductPage from './views/DetailProductPage/DetailProductPage';
import CartPage from './views/CartPage/CartPage';

function App() {
  return (
    <Suspense fallback={(<div>Loading...</div>)}>
      {/*Capital page navigation block */}
      <NavBar />
      <div style={{ paddingTop: '75px', minHeight: 'calc(100vh - 80px)' }}>
        <Switch>
          {/*Central page */}
          <Route exact path="/" component={authFunction(LandingPage)} />
          {/*User authorization page */}
          <Route exact path="/login" component={authFunction(LoginPage)} />
          {/*User registration page */}
          <Route exact path="/register" component={authFunction(RegisterPage)} />
          {/*The page of creating new product by authorized user */}
          <Route exact path="/product/upload" component={authFunction(UploadProductPage)} />
          {/*The page of adding product of authorized user to the cart */}
          <Route exact path="/product/:productId" component={authFunction(DetailProductPage)} />
          {/*The page of representing of cart products of authorized user */}
          <Route exact path="/user/cart" component={authFunction(CartPage)} />
        </Switch>
      </div>
       {/*The final block*/}
      <Footer />
    </Suspense>
  );
}

export default App;