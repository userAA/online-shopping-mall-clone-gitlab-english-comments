import React, { useState } from 'react'
import Dropzone from 'react-dropzone'
import Icon from '@ant-design/icons';
import Axios from 'axios';

//the page of loading and removing of product images
function FileUpload(props) {
    const [Images, setImages] = useState([])

    //the function of loading image files[0] with computer
    const onDrop = (files) => {        
        let formData = new FormData();
        const config = {
            header: {'content-type': 'multipart/form-data'}
        }
        if (files) {formData.append("file", files[0])};

        //request to record the image that we will select inside Node Server
        Axios.post('/api/product/uploadImage', formData, config)
            .then(response => {
                if (response.data.success) {
                    //fixing product images
                    setImages([...Images, response.data.image]);
                    //sending all images of products by destination
                    props.refreshFunction([...Images, response.data.image]);
                } else {
                    alert("Failed to save the Image in Server")
                }
        })
    }

    //the function of removing excess image of product
    const onDelete = (image) => {
        //removing selected image of product from list of all his images
        const currentIndex = Images.indexOf(image);
        let newImages = [...Images];
        newImages.splice(currentIndex, 1);

        //fixing product images
        setImages(newImages);
        //sending all images of product by destination
        props.refreshFunction(newImages);
    }

    return (
        <div style={{display: 'flex', justifyContent: 'space-between'}}>
            {/*The zone of transmit of product images with web-pages on screen computer */}
            <Dropzone onDrop={onDrop} multiple={false} maxSize={8000000} >
                {({getRootProps, getInputProps}) => {
                    return (
                        <div style={{width:'300px', height: '240px', border: '1px solid lightgray', 
                                    display: 'flex', alignItems: 'center', justifyContent: 'center'}}
                                    {...getRootProps()}
                        >
                            <input {...getInputProps()} /> 
                            <Icon type="plus" style={{fontSize: '3rem'}}/>                       
                        </div>
                    )
                }}
            </Dropzone>
            {/*displaying images of products */}
            <div style={{display: 'flex', width: '350px', height: '240px', overflowX: 'scroll'}}>
                {Images.map((image,index) => {
                    return (    
                        <div onClick={() => onDelete(image)}>
                            <img 
                                style={{minWidth: '300px', width: '300px', height: '240px'}} 
                                src={`http://localhost:3001/${image}`} 
                                alt={`productImg-${index}`}
                            />
                        </div>
                    )
                })}
            </div>
        </div>
    )
}

export default FileUpload;