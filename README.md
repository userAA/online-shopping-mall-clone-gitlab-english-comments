gitlab page https://gitlab.com/userAA/online-shopping-mall-clone-gitlab-english-comments.git
gitlab comment online-shopping-mall-clone-gitlab-english-comments

project online-shopping-mall-clone (internet shop on react and redux)
technologies used on the frontend:
    antd,
    axios,
    formik,
    moment,
    react,
    react-dom,
    react-dropzone,
    react-icons,
    react-image-gallery,
    react-paypal-express-checkout,
    react-redux,
    react-router-dom,
    react-scripts,
    redux,
    redux-form,
    redux-promise,
    redux-thunk,
    socket.io-client,
    yup.

technologies used on the backend:
    async,
    bcrypt,
    bcryptjs,
    body-parser,
    cookie-parser,
    express,
    jsonwebtoken,
    mongoose,
    multer,
    socket.io.
There is user registration and authorization. Any authorized user creates products according to photo, name, description,
price and country of creating. All products from all users with them images is being located on central page project.
On central page project authorized user can create the cart product clicking on image of every product and sending this product or
a few his units into own cart of products. In cart of products authorized user can remove any products according at his looking.
